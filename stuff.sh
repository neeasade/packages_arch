#stuff.sh
#Use the following command to list explicit packages by name for later.
#pacaur Qq

#install pacaur:
#build yaourt and install:
sudo pacman -S wget curl yajl diffutils gettext;
cd /tmp;
wget https://aur.archlinux.org/packages/ya/yaourt/yaourt.tar.gz;
wget https://aur.archlinux.org/packages/pa/package-query/package-query.tar.gz;
tar -xvf yaourt.tar.gz;
tar -xvf package-query.tar.gz;
rm yaourt.tar.gz;
rm package-query.tar.gz;
cd package-query;
makepkg;
sudo pacman -U package-query-*.pkg.tar.xz;
cd ..;
rm -rf package-query;
cd yaourt;
makepkg;
sudo pacman -U yaourt-*.pkg.tar.xz;
cd ..;
rm -rf yaourt;
cd ~/;

#Uncomment community repo and add black arch:
sudo rm /etc/pacman.conf
sudo cp pacman.conf /etc/pacman.conf

LIST = $(cat packages.txt)
#install the packages listed in the file:
for i in $list;
    do yaourt -S $i --needed --noconfirm;
done

#configuration:
cd ~
git clone http://github.com/neeasade/.dotfiles
cd dotfiles
./deploy.sh
